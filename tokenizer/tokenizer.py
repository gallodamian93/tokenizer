

import re
import copy


CONST_MIN = 3
CONST_MAX = 15

def tokenizar(text):
    text = toLowerCase(text)
    text = replaceAccentMark(text)
    text = removeNonAlphanum(text)
    text = removeNumbers(text)
    tokens = text.split()
    tokens = filterByLength(tokens, CONST_MIN, CONST_MAX)
    return tokens
#lista tokens = todo
#lista terminos = tokens sin palabras vacias

def toUnicode(text):
    return unidecode(str(text))

def sacar_palabras_vacias(dic_tokens, lista_vacias):
    for v in lista_vacias:
        if(v in dic_tokens):
            del dic_tokens[v]
    return dic_tokens

def toLowerCase(text):
    return text.lower()

def replaceAccentMark(text):
    a,b = 'áéíóúüâêîôû','aeiouuaeiou'
    trans = str.maketrans(a,b)
    return text.translate(trans)
    
def removeNonAlphanum(text):
	return ''.join([i if i.isalnum() else ' ' for i in text])

def removeNumbers(text):
    a,b = '0123456789','          '
    trans = str.maketrans(a,b)
    return text.translate(trans)

def filterByLength(tokens, c_min, c_max):
    aux = copy.deepcopy(tokens)
    for t in tokens:
        if((len(t)<c_min) or (len(t)>c_max)):
            #aux.remove(t)
            aux = list(filter((t).__ne__, aux))
    return aux

#no la uso, necesito la frecuencia
def removeRepeated(tokens):
    return list(set(tokens))

#reemplazada por removeNonAlphanum
def removeSigns(text):
    invalidChars = '.:,;@"|¬°"#$%&/\()=¿^´`?¡!*[]{}_-+<>…~-“«'
    a,b = invalidChars+"'", '                                          '
    trans = str.maketrans(a,b)
    return text.translate(trans)
    
def get_abbrs(text):
    acr = re.compile("(?i)(?:^|(?<= ))(?:[a-z]\.)+[a-z]\.?(?= |$)")
    acrs = acr.findall(text)
    acr2 = re.compile(r"\b([A-Z]{2,4})\b")
    acrs2 = acr2.findall(text)
    acr3 = re.compile(r"\b[A-Za-z]{2,4}\.")
    acrs3 = acr3.findall(text)
    abbrs = acrs + acrs2 + acrs3
    return abbrs

def get_mails(text):
    mail = re.compile("\S+@\S+")
    mails = mail.findall(text)
    return mails
    
def get_urls(text):
    url = re.compile(r'https?://[^\s<>"]+|www\.[^\s<>"]+')
    urls = url.findall(text)
    return urls

def get_nums(text):
    fraction = re.compile(r'(\d+/\d+)')
    fractions = fraction.findall(text)
    percent = re.compile(r'([+\-]?\d+\.?\d*%)')
    percents = percent.findall(text)
    decimal = re.compile(r'[-+]?\d*\.\d+|\d+')
    decimals = decimal.findall(text)
    nums = fractions + percents + decimals
    return nums
    
def get_propios(text):
    propio = re.compile(r'(?=((?<![A-Za-z])[A-Z][a-z]*[\s-][A-Z][a-z]*))')
    propios = propio.findall(text)
    return propios
