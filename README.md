## Sobre el proyecto
El tokenizador extrae los términos contenidos en un conjunto de documentos y los presenta en un archivo de salida, además de estadísticas generadas por el proceso.