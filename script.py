import sys
from argparse import ArgumentParser
import os
sys.path.append(os.path.join(sys.path[0], 'tokenizer'))
import tokenizer as tok
import copy
from collections import OrderedDict
from operator import itemgetter
import codecs
from nltk.stem import SnowballStemmer
from nltk.tokenize import sent_tokenize, word_tokenize

parser = ArgumentParser()
parser.add_argument("-d", "--dir", help="direccion relativa de la coleccion de documentos")
parser.add_argument("-r", "--rem", help="booleano para indicar si se deben remover las palabras vacias")
parser.add_argument("-f", "--fout", help="nombre de archivo que contiene las palabras vacias")

def loadStopWords():
    stopWordsList = []
    with codecs.open("stopWords.txt", "r", "utf-8") as f:
        for line in f:
            stopWordsList.append(line.strip())
        f.close()
    return stopWordsList


def str2bool(arg):
    if(arg):
        return arg.lower() in ('yes', 'true', '1', 't', 'v', 'verdadero')
    else:
        return bool(False)

def checkParams(args):
    if not os.path.isdir(str(args.dir)):
        exit("Parametro 'd' invalido")
    if(str2bool(args.rem)):
        if not(args.fout):
            exit("Parametro 'f' requerido")

def docs2OneFile(args):
    fin = codecs.open("temp.txt", "w", "utf-8")
    for filename in os.listdir(args.dir):
        print("Leyendo " + filename)
        f = open(args.dir+"/"+filename, mode='r', encoding='utf-8', errors='ignore')
        fin.write(f.read())
        f.close()
    fin.close()
    print("Archivo temp.txt creado")

def list2file(filename, items):
    f = codecs.open(filename, "w", "utf-8")
    for item in items:
        f.write(item+"\n")
    f.close()


def generateFrecuenciasFile(terminos):
    try:
        top10 = list(terminos.items())[:10]
        f = codecs.open("frecuencias.txt", "w", "utf-8")
        for t in top10:
            f.write(t[0] + "\t" + str(t[1][0]) + "\n")
        f.write("\n")
        last10pos = len(terminos.items()) -10
        last10 = list(terminos.items())[last10pos:]
        for t in last10:
            f.write(t[0] + "\t" + str(t[1][0]) + "\n")
        print("Archivo 'frecuencias.txt' generado")
    except Exception as e:
        print (e)
        print("No se pudo generar el archivo 'frecuencias.txt'")

def generateTerminosFile(terminos):
    try:
        f = codecs.open("terminos.txt", "w", "utf-8")
        for t in terminos:
            f.write(t + "\t" + str(getCF(terminos[t])) + "\t" + str(getDF(terminos[t])) + "\n")
        print("Archivo 'terminos.txt' generado")
    except Exception as e:
        print("No se pudo generar el archivo 'terminos.txt'")

def generateEstadisticasFile(docs, nTokens, nTerminos, avgTokens, avgTerminos, avgTermLen, snTokens, snTerminos,lnTokens, lnTerminos, nUniqueTerms):
    try:
        f = codecs.open("estadisticas.txt", "w", "utf-8")
        f.write(str(docs) + "\n")
        f.write(str(nTokens) + "\t" + str(nTerminos) + "\n")
        f.write(str(avgTokens) + "\n")
        f.write(str(avgTerminos) + "\t" + str(avgTermLen) + "\n")
        f.write(str(snTokens) + "\t" + str(snTerminos) + "\t" + str(lnTokens) + "\t" + str(lnTerminos) + "\n")
        f.write(str(nUniqueTerms))
        print("Archivo 'estadisticas.txt' generado")
    except Exception as e:
        print("No se pudo generar el archivo 'estadisticas.txt'")    

def generateRegexFiles(abbrs, mails, urls, nums, propios):
    types = (("abreviaturas.txt", abbrs), ("mails.txt", mails), ("urls.txt", urls), ("nums.txt", nums), ("propios.txt", propios))
    for t in types:
        try:
            f = codecs.open(t[0], "w", "utf-8")
            for term in t[1]:
                f.write(str(term) + "\n")
            f.close()
            print("Archivo " + str(t[0]) + " generado")
        except:
            print("No se pudo generar el archivo " + str(t[0]))

def incCF(item):
    item[0] += 1
    return item

def incDF(item):
    item[1] += 1
    return item
    
def getCF(item):
    return item[0]
    
def getDF(item):
    return item[1]

def applyStemming(tokens):
    ss = SnowballStemmer('spanish')
    new_list = copy.deepcopy(tokens)
    for token in tokens:
        new_list.append(ss.stem(token))
    return new_list

def start(vacias):
    abbrs, mails, urls, nums, propios = set(), set(), set(), set(), set()
    tokens_data = {}
    processedDocs = 0
    smallestDocFilename = ""
    largestDocFilename = ""
    smallestDocLength = 9999999
    largestDocLength = -1
    tokenListSmallestDoc = {}
    tokenListLargestDoc = {}
    for filename in os.listdir(args.dir):
        print("Procesando archivo '" + filename + "'")
        f = open(args.dir+"/"+filename, mode='r', encoding='utf-8', errors='ignore')
        fread = f.read()
        new_abbrs = tok.get_abbrs(fread)
        new_mails = tok.get_mails(fread)
        new_urls = tok.get_urls(fread)
        new_nums = tok.get_nums(fread)
        new_propios = tok.get_propios(fread)
        abbrs = abbrs.union(set(new_abbrs))
        mails = mails.union(set(new_mails))
        urls = urls.union(set(new_urls))
        nums = nums.union(set(new_nums))
        propios = propios.union(set(new_propios))
        extracted = new_abbrs + new_mails + new_urls + new_nums + new_propios
        fileLen = len(fread)
        if(fileLen < smallestDocLength):
            smallestDocFilename = filename
            smallestDocLength = fileLen
        if(fileLen > largestDocLength):
            largestDocFilename = filename
            largestDocLength = fileLen
        lista_tokens = tok.tokenizar(fread)
        lista_tokens = applyStemming(lista_tokens)
        lista_tokens += extracted
        
        #procesamiento de tokens
        encontrados = {} #mismo token en mismo documento
        for t in lista_tokens:
            encontro = False
            if (t in tokens_data):
                incCF(tokens_data[t])
                encontro = True
                if(t not in encontrados):
                    incDF(tokens_data[t])
                    encontrados[t] = 0
            if (not encontro):
                tokens_data[t] = [1,1]
                encontrados[t] = 0
        if (filename == smallestDocFilename):
            tokenListSmallestDoc = copy.deepcopy(encontrados)
        if (filename == largestDocFilename):
            tokenListLargestDoc = copy.deepcopy(encontrados)
        processedDocs += 1
    nTokens = len(tokens_data)
    #ya tengo los tokens, ahora necesito los terminos
    lista_terminos = tok.sacar_palabras_vacias(tokens_data, vacias)
    #continuo extrayendo datos para la formulacion de los archivos de salida
    nTerminos = len(lista_terminos)
    promExtractedTokens = nTokens/processedDocs
    promExtractedTerminos = nTerminos/processedDocs
    sumTerminosLength = 0
    uniqueTerms = 0
    for te in lista_terminos:
        sumTerminosLength += len(te)
        if(getCF(lista_terminos[te]) == 1):
            uniqueTerms += 1
    promTerminosLength = sumTerminosLength/nTerminos
    lista_terminos = OrderedDict(sorted(lista_terminos.items(), key=itemgetter(1), reverse=True)) #sort lista_terminos
    terminosSmallestDoc = tok.sacar_palabras_vacias(tokenListSmallestDoc, vacias)
    terminosLargestDoc = tok.sacar_palabras_vacias(tokenListLargestDoc, vacias)
    nTerminosSmallestDoc = len(terminosSmallestDoc)
    nTerminosLargestDoc = len(terminosLargestDoc)
    generateEstadisticasFile(processedDocs, nTokens, nTerminos, promExtractedTokens, promExtractedTerminos, promTerminosLength, len(tokenListSmallestDoc), nTerminosSmallestDoc, len(tokenListLargestDoc), nTerminosLargestDoc, uniqueTerms)
    generateTerminosFile(lista_terminos)
    generateFrecuenciasFile(lista_terminos)
    generateRegexFiles(abbrs, mails, urls, nums, propios)
    return tokens_data, lista_terminos

#inicio de ejecucion
args = parser.parse_args()
checkParams(args)
f_vacias = loadStopWords()
tokens, terminos = start(f_vacias)


"""
a) Un archivo (terminos.txt) con la lista de t´erminos a indexar (ordenado), su frecuencia en la colecci´on
y su DF (Document Frequency).
b) Un segundo archivo (estadisticas.txt) con los siguientes datos:
    1) Cantidad de documentos procesados
    2) Cantidad de tokens y t´erminos extra´ıdos
    3) Promedio de tokens y t´erminos de los documento
    4) Largo promedio de un t´ermino
    5) Cantidad de tokens y t´erminos del documento m´as corto y del m´as largo1
    6) Cantidad de t´erminos que aparecen s´olo 1 vez en la colecci´on
c) Un tercer archivo (frecuencias.txt) con:.
    1) La lista de los 10 t´erminos m´as frecuentes y su CF (Collection Frequency)
    2) La lista de los 10 t´erminos menos frecuentes y su CF.
    
        print("### Estadisticas.txt ###")
    print("Documentos procesados: " + str(processedDocs))
    print("Cantidad de tokens: " + str(nTokens))
    print("Cantidad de terminos: " + str(nTerminos))
    print("Promedio de tokens: " + str(promExtractedTokens))
    print("Promedio de termino: " + str(promExtractedTerminos))
    print("Largo promedio de un termino: " + str(promTerminosLength))
    print("Tokens documento mas corto: " + str(len(tokenListSmallestDoc)))
    print("Terminos documento mas corto: " + str(nTerminosSmallestDoc))
    print("Tokens documento mas largo: " + str(len(tokenListLargestDoc)))
    print("Terminos documento mas largo: " + str(nTerminosLargestDoc))
    print("Terminos que aparecen una sola vez: " + str(uniqueTerms))
    print("Documento mas corto: " + smallestDocFilename)
    print("Documento mas largo: " + largestDocFilename)
    
"""
